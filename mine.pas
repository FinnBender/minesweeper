program Mine;
uses Termio, CRT;

const
   STDIN_FILENO = 0;
   CTRL_C = Chr(3);
   CTRL_D = Chr(4);

   TERM_ESC = Chr(27);

type
   TMark = (Closed, Opened, Flagged);

   TCell = record
      Bomb: Boolean;
      Mark: TMark;
      AdjBombs: Integer;
   end;

   TState = (NotInitialized, Playing, Lost, Won);

   TField = record
      State: TState;
      Rows, Cols: Integer;
      CurRow, CurCol: Integer;
      Cells: array of TCell;
      BombsLeft: Integer;
   end;

   function FieldGetCell(Field: TField; Row, Col: Integer): TCell;
   begin
      Exit(Field.Cells[Row * Field.Cols + Col]);
   end;

   procedure FieldSetCell(var Field: TField; Row, Col: Integer; Cell: TCell);
   begin
      Field.Cells[Row * Field.Cols + Col] := Cell;
   end;

   function FieldHasCell(Field: TField; Row, Col: Integer): Boolean;
   begin
      Exit((Row >= 0)
           and (Col >= 0)
           and (Row < Field.Rows)
           and (Col < Field.Cols));
   end;

   function EmptyField(Rows, Cols: Integer): TField;
   var
      Field: TField;
   begin
      Field.Rows := Rows;
      Field.Cols := Cols;
      Field.CurRow := Rows div 2;
      Field.CurCol := Cols div 2;
      Field.State := NotInitialized;
      SetLength(Field.Cells, Rows * Cols);
      Exit(Field);
   end;

   procedure FieldPlaceBomb(var Field: TField; Row, Col: Integer);
   var
      Cell: TCell;
      NborRow, NborCol: Integer;
   begin
      Cell := FieldGetCell(Field, Row, Col);
      Cell.Bomb := True;
      FieldSetCell(Field, Row, Col, Cell);

      for NborRow := Row-1 to Row+1 do
         for NborCol := Col-1 to Col+1 do
            if FieldHasCell(Field, NborRow, NborCol) then
            begin
               Cell := FieldGetCell(Field, NborRow, NborCol);
               Cell.AdjBombs := Cell.AdjBombs + 1;
               FieldSetCell(Field, NborRow, NborCol, Cell);
            end;
   end;

   procedure FieldPlaceBombs(var Field: TField; Bombs: Integer);
   var
      Placed, Visited, Total: Integer;
      Row, Col: Integer;
   begin
      Placed := 0;
      Visited := 0;
      Total := Field.Rows * Field.Cols - 1;
      for Row := 0 to Field.Rows - 1 do
         for Col := 0 to Field.Cols - 1 do
         begin
            if (Row = Field.CurRow) and (Col = Field.CurCol) then continue;
            if (random < (Bombs - Placed) / (Total - Visited)) then
            begin
               FieldPlaceBomb(Field, Row, Col);
               Placed := Placed + 1;
            end;

            Visited := Visited + 1;
         end;
   end;

   procedure FieldInitialize(var Field: TField);
   begin
      if Field.State = NotInitialized then
      begin
         FieldPlaceBombs(Field, Field.BombsLeft);
         Field.State := Playing;
      end;
   end;

   procedure FieldCheckWon(var Field: TField);
   begin
      if Field.BombsLeft = 0 then
         Field.State := Won;
   end;

   procedure FieldOpenCell(var Field: TField; Row, Col: Integer);
   var
      Cell: TCell;
      NborRow, NborCol: Integer;
   begin
      FieldInitialize(Field);

      if not FieldHasCell(Field, Row, Col) then Exit();
      Cell := FieldGetCell(Field, Row, Col);

      if Cell.Mark = Opened then Exit()
      else if Cell.Bomb then Field.State := Lost;

      Cell.Mark := Opened;
      FieldSetCell(Field, Row, Col, Cell);

      if Cell.AdjBombs = 0 then
            for NborRow := Row - 1 to Row + 1 do
               for NborCol := Col - 1 to Col + 1 do
                  if not ((NborRow = Row) and (NborCol = Col)) then
                     FieldOpenCell(Field, NborRow, NborCol);
   end;

   procedure FieldOpenAllCells(var Field: TField);
   var
      Row, Col: Integer;
   begin
      for Row := 0 to Field.Rows - 1 do
         for Col := 0 to Field.Cols - 1 do
            FieldOpenCell(Field, Row, Col);

   end;

   procedure FieldFlagCell(var Field: TField; Row, Col: Integer);
   var
      Cell: TCell;
   begin
      FieldInitialize(Field);

      Cell := FieldGetCell(Field, Row, Col);
      case Cell.Mark of
         Closed:
            begin
               Cell.Mark := Flagged;
               Field.BombsLeft := Field.BombsLeft - 1;
            end;
         Flagged:
            begin
               Cell.Mark := Closed;
               Field.BombsLeft := Field.BombsLeft + 1;
            end;
      end;
      FieldSetCell(Field, Row, Col, Cell);

      if Field.BombsLeft = 0 then
      begin
         FieldOpenAllCells(Field);
         if Field.State <> Lost then
            Field.State := Won;
      end;
   end;

   procedure WriteField(Field: TField);
   var
      Row, Col: Integer;
      Cell: TCell;
   begin
      for Row := 0 to Field.Rows - 1 do
      begin
         for Col := 0 to Field.Cols - 1 do
         begin
            if (Row = Field.CurRow) and (Col = Field.CurCol) then
               Write('[')
            else
               Write(' ');

            Cell := FieldGetCell(Field, Row, Col);
            case (Cell.Mark) of
               Closed: Write('.');
               Flagged: Write('F');
               Opened: if Cell.Bomb then Write('@')
                       else if Cell.AdjBombs > 0 then Write(Cell.AdjBombs)
                       else Write(' ');
            end;

            if (Row = Field.CurRow) and (Col = Field.CurCol) then
               Write(']')
            else
               Write(' ');

         end;

         if Row = 1 then Write(' Bombs: ', Field.BombsLeft:2);
         WriteLn('');
      end;
   end;

   function Clamp(x, a, b: Integer): Integer;
   begin
      if x > b then Exit(b);
      if x < a then Exit(a);
      Exit(x);
   end;

var
   old_tattr: Termios;
   Field: TField;
   ch: Char;

   procedure RestoreTerminal;
   begin
      TCSetAttr(STDIN_FILENO, TCSANOW, old_tattr);
   end;

   procedure SetupTerminal;
   var
      tattr: Termios;
   begin
      if IsATTY(STDIN_FILENO) = 0 then
      begin
         WriteLn(StdErr, 'ERROR: not a terminal');
         Halt(0);
      end;

      TCGetAttr(STDIN_FILENO, old_tattr);
      TCGetAttr(STDIN_FILENO, tattr);
      tattr.c_lflag := tattr.c_lflag and not (ICANON or ECHO);
      tattr.c_cc[VMIN] := 1;
      tattr.c_cc[VTIME] := 0;
      TCSetAttr(STDIN_FILENO, TCSADRAIN, tattr);
      AddExitProc(@RestoreTerminal);
   end;

begin
   SetupTerminal;
   Randomize;

   Field := EmptyField(9, 13);
   Field.BombsLeft := 15;
   WriteField(Field);

   while (Field.State <> Won) and (Field.State <> Lost) do
   begin
      ch := ReadKey;
      case ch of
        'w': Field.CurRow := Clamp(Field.CurRow-1, 0, Field.Rows-1);
        's': Field.CurRow := Clamp(Field.CurRow+1, 0, Field.Rows-1);
        'a': Field.CurCol := Clamp(Field.CurCol-1, 0, Field.Cols-1);
        'd': Field.CurCol := Clamp(Field.CurCol+1, 0, Field.Cols-1);
        'f': FieldFlagCell(Field, Field.CurRow, Field.CurCol);
        ' ': FieldOpenCell(Field, Field.CurRow, Field.CurCol);
        CTRL_C, CTRL_D: Halt(0);
      end;

      Write(TERM_ESC, '[', Field.Rows,   'A');
      Write(TERM_ESC, '[', Field.Cols*3, 'D');
      WriteField(Field);
   end;

   if Field.State = Lost then
      WriteLn('You lost!')
   else if Field.State = Won then
      WriteLn('You won!');
end.

