# mine
A minesweeper implementation written in [Free Pascal](https://www.freepascal.org/).

## Quickstart

```console
$ fpc mine.pas
$ ./mine
```

## Controls

| Key   | Action      |
| ----- | ----------- |
| WASD  | Move cursor |
| F     | Flag cell   |
| Space | Open cell   |
| C-c   | Quit        |
